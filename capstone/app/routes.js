const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		let currency = Object.entries(exchangeRates)
		let foundAlias = currency.find((er) => {
			return er.alias === req.body.alias
		})
		// let foundAlias = exchangeRates
		// .find((er) => {
		// 	return er.alias === req.body.alias
		// });

		// if(req.body !== null){
		// 	return res.status(200).send({
		// 		'message': 'Welcome!'
		// 	})
		// }

		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter NAME'
			})
		}

		if(typeof req.body.name !== "string"){
			return res.status(400).send({
				'message': 'Bad Request - invalid input for NAME'
			})
		}

		if(req.body.name.length === 0){
			return res.status(400).send({
				'message': 'Bad Request - NAME is required'
			})	
		}

		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error': 'Bad Request - invalid input for EX'
			})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX'
			})
		}

		if(req.body.ex === null){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX'
			})
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter alias'
			})
		}

		if(typeof req.body.alias !== "string"){
			return res.status(400).send({
				'message': 'Bad Request - invalid input for ALIAS'
			})
		}

		if(req.body.alias.length === 0){
			return res.status(400).send({
				'message': 'Bad Request - ALIAS is required'
			})
		}

		if(!foundAlias){
			return res.status(200).send({
				alias: req.body.alias,
				name: exchangeRates.name,
				ex: exchangeRates.ex
			})
		}

		if(foundAlias){
			return res.status(400).send({
				'error': 'Bad Request - duplicate ALIAS found'
			})
		}
	})


}
