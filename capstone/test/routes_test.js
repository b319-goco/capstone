const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})
	
	it('test_api_post_curreny_is_200', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "riyadh",
			name: "Saudi Arabian Riyadh",
			ex: {
				'peso': 0.47,
	        	'usd': 0.0092,
	        	'yen': 10.93,
	        	'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.not.equal(400);
			// done();
		})		
	})

	it('test_api_post_returns_400_if_no_curreny_name', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "riyadh",
			ex: {
				'peso': 0.47,
	        	'usd': 0.0092,
	        	'yen': 10.93,
	        	'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			// done();
		})		
	})

	it('test_api_post_returns_400_if_curreny_name_is_not_a_string', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "riyadh",
			name: 9090,
			ex: {
				'peso': 0.47,
	        	'usd': 0.0092,
	        	'yen': 10.93,
	        	'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			// done();
		})		
	})
	
	it('test_api_post_returns_400_if_curreny_name_is_an_empty_string', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "riyadh",
			name: "",
			ex: {
				'peso': 0.47,
	        	'usd': 0.0092,
	        	'yen': 10.93,
	        	'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			// done();
		})		
	})
	
	it('test_api_post_returns_400_if_curreny_ex_not_an_object', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "riyadh",
			name: "Saudi Arabian Riyadh",
			ex: 9090
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			//done();
		})
	})
	
	it('test_api_post_returns_400_if_no_ex_content', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "riyadh",
			name: "Saudi Arabian Riyadh",
			ex: {}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			// done();
		})
	})
	
	it('test_api_post_returns_400_if_no_currency_alias', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "Saudi Arabian Riyadh",
			ex: {
				'peso': 0.47,
	        	'usd': 0.0092,
	        	'yen': 10.93,
	        	'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			//done();
		})
	})
	
	it('test_api_post_returns_400_if_currency_alias_not_a_string', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: 9090,
			name: "Saudi Arabian Riyadh",
			ex: {
				'peso': 0.47,
	        	'usd': 0.0092,
	        	'yen': 10.93,
	        	'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			// done();
		})
	})
	
	it('test_api_post_returns_400_if_currency_alias_is_an_empty_string', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "",
			name: "Saudi Arabian Riyadh",
			ex: {
				'peso': 0.47,
	        	'usd': 0.0092,
	        	'yen': 10.93,
	        	'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			// done();
		})
	})

	it('test_api_post_returns_400_if_duplicate_alias_found', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "usd",
			name: "United States Dollar",
			ex: {
				'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			// done();
		})
	})

	it('test_api_post_returns_submitted_object_to_show_submission_was_written', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "riyadh",
			name: "Saudi Arabian Riyadh",
			ex: {
				'peso': 0.47,
	        	'usd': 0.0092,
	        	'yen': 10.93,
	        	'yuan': 0.065
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			// done();
		})
	})
})
